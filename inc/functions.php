<?php
error_reporting(E_ERROR | E_PARSE);
require_once('dbinfo.inc');
require_once('constants.inc');

class pos_functions{
	private function connect()
    {
		$link = mysqli_connect(HOSTNAME, USERNAME, PASSWORD, DATABASE)
		    or die('Could not connect: ' . mysqli_error());
		mysqli_select_db($link,DATABASE) or die('Could not select database' . mysql_error());
		return $link;
    }

    //VIEW FUNCTIONS
    
    public function showBestSeller($d1,$d2){
        $link = $this->connect();
        $query2=sprintf("SELECT F.FOOD_NAME,
                                SUM(OI.QTY)
                        FROM ORDERS O
                        INNER JOIN ORDER_ITEMS OI
                        ON OI.ORDER_ID = O.ORDER_ID 
                        INNER JOIN FOOD F
                        ON F.FOOD_ID = OI.FOOD_ID
                        WHERE O.STATUS = 2
                        AND O.ORDER_DATE BETWEEN '".$d1."' AND '".$d2."'
                        GROUP BY F.FOOD_ID
                        ORDER BY SUM(OI.QTY) DESC
                        LIMIT 10

                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            print "<tr><td>" . $row[0] . "</td>";
            print "<td>" . $row[1] . "</td></tr>";
        }
    }
    public function showSalesOrders(){
        $link = $this->connect();
        $query2=sprintf("SELECT O.ORDER_DATE,
                                O.ORDER_ID,
                                C.CUSTOMER_NAME,
                                O.TABLE_NO,
                                SUM(OI.TOTAL_AMT),
                                S.STATUS_NAME
                        FROM ORDERS O
                        INNER JOIN ORDER_ITEMS OI
                        ON OI.ORDER_ID = O.ORDER_ID 
                        INNER JOIN CUSTOMER C
                        ON C.CUSTOMER_ID = O.CUSTOMER_ID
                        INNER JOIN STATUS S
                        ON S.STATUS_ID = O.STATUS
                        WHERE O.STATUS = 1
                        GROUP BY O.ORDER_ID
                        ORDER BY O.ORDER_ID
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            print "<tr><td>" . $row[0] . "</td>";
            print "<td><a href=vieworderdetails.php?oid=" . $row[1] . ">".$row[1]."</a></td>";
            print "<td>" . $row[2] . "</td>";
            print "<td><a href=vieworderdetails.php?oid=" . $row[1] . ">" . $row[3] . "</td>";
            print "<td>" . number_format($row[4],2) . "</td>";
            print "<td>" . $row[5] . "</td></tr>";
        }
    }
    public function showSales(){
        $link = $this->connect();
        $query2=sprintf("SELECT O.ORDER_DATE,
                                O.ORDER_ID,
                                C.CUSTOMER_NAME,
                                O.TABLE_NO,
                                SUM(OI.TOTAL_AMT),
                                S.STATUS_NAME
                        FROM ORDERS O
                        INNER JOIN ORDER_ITEMS OI
                        ON OI.ORDER_ID = O.ORDER_ID 
                        INNER JOIN CUSTOMER C
                        ON C.CUSTOMER_ID = O.CUSTOMER_ID
                        INNER JOIN STATUS S
                        ON S.STATUS_ID = O.STATUS
                        WHERE O.STATUS = 2
                        GROUP BY O.ORDER_ID
                        ORDER BY O.ORDER_ID
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            print "<tr><td>" . $row[0] . "</td>";
            print "<td><a href=viewsaledetails.php?oid=" . $row[1] . ">".$row[1]."</a></td>";
            print "<td>" . $row[2] . "</td>";
            print "<td><a href=viewsaledetails.php?oid=" . $row[1] . ">" . $row[3] . "</td>";
            print "<td>" . number_format($row[4],2) . "</td>";
            print "<td>" . $row[5] . "</td></tr>";
        }
    }
    public function addAtitle($a){
        $link = $this->connect();

        $query=sprintf("INSERT INTO ACCOUNT_TITLES(ACCOUNT_TITLE) VALUES ('%s');",

        mysqli_real_escape_string($link,$a));

        $result = mysqli_query($link,$query) or die('Query failed: ' . mysqli_error($link));
        if(mysqli_error($result)){
            
           print '
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <b>Alert!</b> Error in Adding an Account Title
            </div>
        ';
        }else{
            print '
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <b>Alert!</b> Successfully added an Account Title
                </div>
            ';
        }
    }
    public function addExpense($edate,$amt,$atitle,$remarks){
        $link = $this->connect();

        $query=sprintf("INSERT INTO EXPENSES(EDATE,ACCOUNT_TITLE,AMOUNT,REMARKS) VALUES ('%s','%s','%s','%s');",

        mysqli_real_escape_string($link,$edate),
        mysqli_real_escape_string($link,$atitle),
        mysqli_real_escape_string($link,$amt),
        mysqli_real_escape_string($link,$remarks));

        $result = mysqli_query($link,$query) or die('Query failed: ' . mysqli_error($link));
        if(mysqli_error($result)){
            
           print '
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <b>Alert!</b> Error in Adding Expense
            </div>
        ';
        }else{
            print '
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <b>Alert!</b> Successfully added an Expense
                </div>
            ';
        }
    }
    public function addFood($fname,$fsrp){
        $link = $this->connect();

        $query=sprintf("INSERT INTO FOOD(FOOD_NAME,FOOD_SRP) VALUES ('%s','%s');",

        mysqli_real_escape_string($link,$fname),
        mysqli_real_escape_string($link,$fsrp));

        $result = mysqli_query($link,$query) or die('Query failed: ' . mysqli_error($link));
        if(mysqli_error($result)){
            
           print '
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <b>Alert!</b> Error in Adding Food
            </div>
        ';
        }else{
            print '
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <b>Alert!</b> Successfully added a new Food
                </div>
            ';
        }
    }
    public function addSale($oid){
        $link = $this->connect();

        $query=sprintf("UPDATE ORDERS
                        SET STATUS = 2
                        WHERE ORDER_ID = '%s';",

        mysqli_real_escape_string($link,$oid));

        $result = mysqli_query($link,$query) or die('Query failed: ' . mysqli_error($link));
        if(mysqli_error($result)){
            
           print '
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <b>Alert!</b> Error in Adding Sale
            </div>
        ';
        }else{
            print '
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <b>Alert!</b> Successfully added a new Sale
                </div>
            ';
        }
    }

    public function cancelOrder($oid){
        $link = $this->connect();

        $query=sprintf("UPDATE ORDERS
                        SET STATUS = 3
                        WHERE ORDER_ID = '%s';",

        mysqli_real_escape_string($link,$oid));

        $result = mysqli_query($link,$query) or die('Query failed: ' . mysqli_error($link));
        if(mysqli_error($result)){
            
           print '
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <b>Alert!</b> Error in Cancelling Order
            </div>
        ';
        }else{
            print '
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <b>Alert!</b> Successfully Cancelled an Order
                </div>
            ';
        }
    }

    public function addOrder($odate,$custid,$tableno){
        $link = $this->connect();

        $query=sprintf("INSERT INTO ORDERS(ORDER_DATE,CUSTOMER_ID,TABLE_NO,STATUS) VALUES ('%s','%s','%s',1);",

        mysqli_real_escape_string($link,$odate),
        mysqli_real_escape_string($link,$custid),
        mysqli_real_escape_string($link,$tableno));

        $result = mysqli_query($link,$query) or die('Query failed: ' . mysqli_error($link));
        
    }

    public function addOrderItems($oid,$fname,$fqty,$fsrp,$famount){
        $link = $this->connect();

        for($i=0;$i<sizeof($fname);$i++){
            $query=sprintf("INSERT INTO ORDER_ITEMS(ORDER_ID,FOOD_ID,QTY,AMT,TOTAL_AMT) VALUES ('%s','%s','%s','%s','%s');",

            mysqli_real_escape_string($link,$oid),
            mysqli_real_escape_string($link,$fname[$i]),
            mysqli_real_escape_string($link,$fqty[$i]),
            mysqli_real_escape_string($link,$fsrp[$i]),
            mysqli_real_escape_string($link,$famount[$i]));

            $result = mysqli_query($link,$query) or die('Query failed: ' . mysqli_error($link));
        }

        
        if(mysqli_error($result)){
            
           print '
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <b>Alert!</b> Error in Adding Order
            </div>
        ';
        }else{
            print '
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <b>Alert!</b> Successfully added a new Order
                </div>
            ';
        }
    }

    public function addCustomer($cname){
        $link = $this->connect();

        $query=sprintf("INSERT INTO CUSTOMER(CUSTOMER_NAME) VALUES ('%s');",

        mysqli_real_escape_string($link,$cname));

        $result = mysqli_query($link,$query) or die('Query failed: ' . mysqli_error($link));
        
    }
    public function getMaxCustId(){
        $link = $this->connect();
        $query2=sprintf("SELECT MAX(CUSTOMER_ID)
                        FROM CUSTOMER
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            return $row[0];
        }
    }
    public function getMaxOrderId(){
        $link = $this->connect();
        $query2=sprintf("SELECT MAX(ORDER_ID)
                        FROM ORDERS
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            return $row[0];
        }
    }
    public function updateFoodSRP($fid,$fsrp){
        $link = $this->connect();

        $query=sprintf("UPDATE FOOD
                        SET FOOD_SRP = %s
                        WHERE FOOD_ID = %s",

        mysqli_real_escape_string($link,$fsrp),
        mysqli_real_escape_string($link,$fid));

        $result = mysqli_query($link,$query) or die('Query failed: ' . mysqli_error($link));
        if(mysqli_error($result)){
            
           print '
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <b>Alert!</b> Error in Updating Food SRP
            </div>
        ';
        }else{
            print '
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <b>Alert!</b> Successfully updated Food SRP
                </div>
            ';
        }
    }
    public function showMenu(){
        $link = $this->connect();
        $query2=sprintf("SELECT FOOD_NAME,
                                FOOD_SRP,
                                FOOD_ID
                        FROM FOOD
                        ORDER BY FOOD_NAME
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            print "<tr><td>" . $row[0] . "</td>";
            print "<td>" . number_format($row[1],2) . "</td>";
            print "<td><a href='viewfood.php?fid=".$row[2]."'>Update SRP</a></td></tr>";
        }
    }
    public function ddlAccountTitles(){
        $link = $this->connect();
        $query2=sprintf("SELECT ID,
                                ACCOUNT_TITLE
                        FROM ACCOUNT_TITLES
                        ORDER BY ACCOUNT_TITLE
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            print "<option value=".$row[0].">".$row[1]."</option>";
        }
    }
    public function populateMenu(){
        $link = $this->connect();
        $query2=sprintf("SELECT F.FOOD_ID,
                                F.FOOD_NAME,
                                F.FOOD_SRP
                        FROM FOOD F
                        ORDER BY F.FOOD_NAME
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            $fname = $this->getFoodDetail($row[0],0);
            $fsrp = $this->getFoodDetail($row[0],1);
            print "<button type='button' onClick='addFoodItem(".$row[0].",\"".$fname."\",\"".number_format($fsrp,2)."\");'>" . $row[1] . " - " . $row[2] . "</button><br /><br />";
        }
    }

    public function showIS($d1,$d2){
        $link = $this->connect();
        $query2=sprintf("SELECT SUM(OI.TOTAL_AMT)
                        FROM ORDER_ITEMS OI
                        INNER JOIN ORDERS O
                        ON O.ORDER_ID = OI.ORDER_ID
                        WHERE O.STATUS = 2
                        AND O.ORDER_DATE BETWEEN '".$d1."' and '".$d2."'
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       print "<table width='100%'><tr><td><strong>INCOME</strong></td></tr>";
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            print "<tr><td><td width='70%'>Sales</td>";
            print "<td width='20%''>".number_format($row[0],2)."</td></tr>";
        }
        print "</table>";
    }
    public function showES($d1,$d2){
        $link = $this->connect();
        $inc = $this->getIncomeCost($d1,$d2);
        $exp = $this->getExpensesCost($d1,$d2);
        $query2=sprintf("SELECT A.ACCOUNT_TITLE,
                                SUM(E.AMOUNT),
                                E.REMARKS
                        FROM EXPENSES E
                        INNER JOIN ACCOUNT_TITLES A
                        ON A.ID = E.ACCOUNT_TITLE
                        WHERE E.EDATE BETWEEN '".$d1."' and '".$d2."'
                        GROUP BY E.ACCOUNT_TITLE
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       print "<table width='100%'><tr><td><strong>EXPENSES</strong></td></tr>";
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            print "<tr><td><td width='70%'>".$row[0]." (".$row[2].")</td>";
            print "<td width='20%''>".number_format($row[1],2)."</td></tr>";
        }
        print "<tr></tr><tr></tr><tr><td><h5><b>TOTAL INCOME</h5></td><td></td><td style='color:red'><b>".number_format($inc,2)."</b></td></tr>";
        print "<tr></tr><tr></tr><tr><td><h5><b>TOTAL EXPENSE</h5></td><td></td><td style='color:red'><b>".number_format($exp,2)."</b></td></tr>";
        print "<tr><td><h5><b></h5></td><td></td><td style='color:red'><b>-------------------</b></td></tr>";
        print "<tr></tr><tr></tr><tr><td><h5><b>NET INCOME</h5></td><td></td><td style='color:red'><b>".number_format($inc-$exp,2)."</b></td></tr>";
        print "</table>";
    }

    public function getExpensesCost($d1,$d2){
        $link = $this->connect();
        $query2=sprintf("SELECT SUM(E.AMOUNT)
                        FROM EXPENSES E
                        INNER JOIN ACCOUNT_TITLES A
                        ON A.ID = E.ACCOUNT_TITLE
                        WHERE E.EDATE BETWEEN '".$d1."' and '".$d2."'
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            return $row[0];
        }
    }
    public function getIncomeCost($d1,$d2){
        $link = $this->connect();
        $query2=sprintf("SELECT SUM(OI.TOTAL_AMT)
                        FROM ORDER_ITEMS OI
                        INNER JOIN ORDERS O
                        ON O.ORDER_ID = OI.ORDER_ID
                        WHERE O.STATUS = 2
                        AND O.ORDER_DATE BETWEEN '".$d1."' and '".$d2."'
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            return $row[0];
        }
    }

    public function getFoodDetail($fid,$i){
        $link = $this->connect();
        $query2=sprintf("SELECT FOOD_NAME,
                                FOOD_SRP
                        FROM FOOD
                        WHERE FOOD_ID = ".$fid."
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            return $row[$i];
        }
    }
    public function getOrderDetail($oid,$i){
        $link = $this->connect();
        $query2=sprintf("SELECT O.ORDER_DATE,
                                C.CUSTOMER_NAME,
                                O.TABLE_NO,
                                SUM(OI.TOTAL_AMT),
                                S.STATUS_NAME
                        FROM ORDERS O
                        INNER JOIN ORDER_ITEMS OI
                        ON OI.ORDER_ID = O.ORDER_ID 
                        INNER JOIN CUSTOMER C
                        ON C.CUSTOMER_ID = O.CUSTOMER_ID
                        INNER JOIN STATUS S
                        ON S.STATUS_ID = O.STATUS
                        WHERE O.ORDER_ID = ".$oid."
                        GROUP BY O.ORDER_ID
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            return $row[$i];
        }
    }

    public function showFoodItems($oid){
        $link = $this->connect();
        $query2=sprintf("SELECT OI.QTY,
                                F.FOOD_NAME,
                                OI.AMT,
                                OI.TOTAL_AMT
                        FROM ORDER_ITEMS OI
                        INNER JOIN FOOD F
                        ON F.FOOD_ID = OI.FOOD_ID
                        WHERE OI.ORDER_ID = $oid
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            print "<tr><td>" . $row[0] . "</td>";
            print "<td>" . $row[1] . "</td>";
            print "<td>" . number_format($row[2],2) . "</td>";
            print "<td>" . number_format($row[3],2) . "</td></tr>";
        }
    }
    public function getDailySales($d1,$d2){
        $diff = abs(strtotime($d2) - strtotime($d1));
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        $income = array();
        for($i=0;$i<($days+1);$i++){
            $d3 = date("Y-m-".($i+1));

            $link = $this->connect();
            $query2=sprintf("SELECT SUM(OI.TOTAL_AMT)
                            FROM ORDER_ITEMS OI
                            INNER JOIN ORDERS O
                            ON O.ORDER_ID = OI.ORDER_ID
                            WHERE O.STATUS = 2
                            AND O.ORDER_DATE = '".$d3."'
                            GROUP BY O.ORDER_DATE,O.ORDER_ID
                    ");
            $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

            //while($row= mysqli_fetch_array($result2, MYSQL_NUM))
            //{
                $row= mysqli_fetch_array($result2, MYSQL_NUM);
                array_push($income, $row[0]);
            //}
        }
        return $income;
        
    }
    public function getDailyExpense($d1,$d2){
        $diff = abs(strtotime($d2) - strtotime($d1));
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        $expense = array();
        for($i=0;$i<($days+1);$i++){
            $d3 = date("Y-m-".($i+1));

            $link = $this->connect();
            $query2=sprintf("SELECT SUM(E.AMOUNT)
                        FROM EXPENSES E
                        WHERE E.EDATE = '".$d3."'
                        GROUP BY E.EDATE
                    ");
            $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

            $row= mysqli_fetch_array($result2, MYSQL_NUM);
                array_push($expense, $row[0]);
       
        }
        return $expense;
    }

    public function getDays($d1,$d2){
        $diff = abs(strtotime($d2) - strtotime($d1));
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        $expense = array();
        for($i=0;$i<($days+1);$i++){
            $d3 = date("Y-m-".($i+1));

            
                array_push($expense, $d3);
       
        }
        return $expense;
    }
    public function getD($d1,$d2){
        $diff = abs(strtotime($d2) - strtotime($d1));
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        
        return $days+1;
    }

    public function getChart2(){
         $link = $this->connect();
        $query2=sprintf("SELECT ACCOUNT_TITLE,
                                AMOUNT
                        FROM EXPENSES
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

       
        while($row= mysqli_fetch_array($result2, MYSQL_NUM))
        {
            $arr2=array_keys($row);
             return $arr2;

        }
    }
    public function login($username,$password){
         $link = $this->connect();
        $query2=sprintf("SELECT access_level
                        FROM user
                        WHERE username = '".$username."'
                        AND password = '".$password."'
                ");

        $result2 = mysqli_query($link,$query2) or die('Query failed: ' . mysqli_error($link));

        if($row= mysqli_fetch_array($result2, MYSQL_NUM)>0){
            $_SESSION['username'] = $username;
            $_SESSION['password'] = md5($password);
            $_SESSION['access'] = $row[0];
            header("location:index.php");
        }else{
            print "INVALID";
        }
        
    }
    public function showHead(){
        print '
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">H4POS - '.COMPANY_NAME.'</a>
            </div>
        ';
    }
}

?>