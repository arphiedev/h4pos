$(function() {

    
    Morris.Area({
                element: 'morris-area-chart',
                data: [
                                {
                    days: '2015-05-01',
                    income: 100,
                    expense: 0,
                },
                                {
                    days: '2015-05-02',
                    income: 0,
                    expense: 0,
                },
                                {
                    days: '2015-05-03',
                    income: 0,
                    expense: 0,
                },
                                {
                    days: '2015-05-15',
                    income: 0,
                    expense: 0,
                },
                                ],
                xkey: 'days',
                ykeys: ['income','expense'],
                labels: ['Income','Expenses'],
                xLabels:['Day'],
                pointSize: 2,
                hideHover: 'auto',
                resize: true
            });

});
