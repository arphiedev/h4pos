<?php

	include '../inc/functions.php';
	$db = new pos_functions();

	
?>
<!DOCTYPE html>
<html>
<head>
	<title>H4 Point Of Sales</title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <!-- MetisMenu CSS -->
    <link href="../css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="../plugins/calendar/css/responsive-calendar.css" rel="stylesheet" type="text/css" />
    <link href="../css/AdminLTE.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		    <?php $db->showHead(); ?>
		    <!-- /.navbar-header -->

		    <ul class="nav navbar-top-links navbar-right">
		        <!-- /.dropdown -->
		        <li class="dropdown">
		            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
		                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
		            </a>
		            <ul class="dropdown-menu dropdown-messages">
		                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
		                </li>
		                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
		                </li>
		                <li class="divider"></li>
		                <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
		                </li>
		            </ul>
		            <!-- /.dropdown-user -->
		        </li>
		        <!-- /.dropdown -->
		    </ul>
		    <!-- /.navbar-top-links -->

		    <div class="navbar-default sidebar" role="navigation">
		        <div class="sidebar-nav navbar-collapse">
		            <ul class="nav" id="side-menu">
		                <li class="sidebar-search">
		                    <div class="input-group custom-search-form">
		                        <input type="text" class="form-control" placeholder="Search...">
		                        <span class="input-group-btn">
		                        <button class="btn btn-default" type="button">
		                            <i class="fa fa-search"></i>
		                        </button>
		                    </span>
		                    </div>
		                    <!-- /input-group -->
		                </li>
		                <li>
		                    <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
		                </li>
		                <li>
		                    <a href="#"><i class="fa fa-cutlery fa-fw"></i> Sales<span class="fa arrow"></span></a>
		                    <ul class="nav nav-second-level">
		                        <li>
		                            <a href="addorder.php">Add Order</a>
		                        </li>
		                        <li>
		                            <a href="vieworders.php">View Orders</a>
		                        </li>
		                        <li>
		                            <a href="viewsales.php">View Sales</a>
		                        </li>
		                    </ul>
		                    <!-- /.nav-second-level -->
		                </li>
		               	<li>
		                    <a href="viewMenu.php"><i class="fa fa-file-text fa-fw"></i> Menu Management</a>
		                </li>
		                <li>
		                    <a href="#"><i class="fa fa-wrench fa-book"></i> Accounting and Statistics<span class="fa arrow"></span></a>
		                    <ul class="nav nav-second-level">
		                        <li>
		                            <a href="addexpense.php">Add Expense</a>
		                        </li>
		                        <li>
		                            <a href="addaccounttitle.php">Add Account Title</a>
		                        </li>
		                        <li>
		                            <a href="incomestatement.php">View Income Statement</a>
		                        </li>
		                        <li>
		                            <a href="stat.php">Statistics</a>
		                        </li>
		                    </ul>
		                    <!-- /.nav-second-level -->
		                </li>
		                <!--li>
		                    <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
		                    <ul class="nav nav-second-level">
		                        <li>
		                            <a href="#">Second Level Item</a>
		                        </li>
		                        <li>
		                            <a href="#">Second Level Item</a>
		                        </li>
		                        <li>
		                            <a href="#">Third Level <span class="fa arrow"></span></a>
		                            <ul class="nav nav-third-level">
		                                <li>
		                                    <a href="#">Third Level Item</a>
		                                </li>
		                                <li>
		                                    <a href="#">Third Level Item</a>
		                                </li>
		                                <li>
		                                    <a href="#">Third Level Item</a>
		                                </li>
		                                <li>
		                                    <a href="#">Third Level Item</a>
		                                </li>
		                            </ul>
		                           
		                        </li>
		                    </ul>
		                    
		                </li-->
		                <li>
		                    <a href="#"><i class="fa fa-users fa-fw"></i> User Management<span class="fa arrow"></span></a>
		                    
		                    <!-- /.nav-second-level -->
		                </li>
		            </ul>
		        </div>
		        <!-- /.sidebar-collapse -->
		    </div>
		    <!-- /.navbar-static-side -->
		</nav>
		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Sales Order</h1>
                    <?php
	                    if(isset($_POST['odate'])){
							$odate = $_POST['odate'];
							$custname = $_POST['custname'];
							$tableno = $_POST['tableno'];
							$db->addCustomer($custname);
							$custid = $db->getMaxCustId();
							$db->addOrder($odate,$custid,$tableno);
							$oid = $db->getMaxOrderId();
							$db->addOrderItems($oid,$_POST['foodname'],$_POST['foodqty'],$_POST['foodprice'],$_POST['foodamount']);

						}
					?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <h4>Total: <b>P<span id="totamt">0.00</span></b></h4>
            

           	<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-plus fa-fw"></i> Add Sales Order
	                        
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	<form method="post" action="addorder.php">
	                            <label for="orderDate">Date</label>
									<input class="form-control" type="date" name="odate" value=<?php print date("Y-m-d"); ?> required/>
									<br />
	                            <label for="cname">Customer Name</label>
									<input class="form-control" type="text" name="custname" required/>
									<br />
								
									<div class="col-lg-4" style="overflow:auto;height:400px;">
									<?php
										$db->populateMenu();
									?>
									</div>
									<div id="nl" class="col-lg-4" ></div>
									<div id="pending_items" class="col-lg-4" style="overflow:auto;height:400px;">
										<div class="dataTable_wrapper">
			                                <table id="pitems" name="oitems" class="table table-striped table-bordered table-hover" id="dataTables-example">
			                                    <thead>
			                                        <tr>
			                                            <th>Item</th>
			                                            <th>Qty</th>
			                                            <th>Price</th>
			                                            <th>Amount</th>
			                                            <th>Action</th>
			                                            <th></th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                        
			                                    </tbody>
			                                </table>
			                            </div>
									</div>
								<label for="cname">Table Number</label>
									<input class="form-control" type="text" name="tableno" required/>
									<br />
									<div class="col-lg-12">
									<input type="submit" class="btn btn-info btn-sm" value="Add Order">
									</div>
							</form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                   
                    <!-- /.panel -->
                </div>
            <!-- /.row -->
            
            <!-- /.row -->

        </div>

	</div>

	<script src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/sb-admin-2.js"></script>
	<script src="../js/metisMenu.min.js"></script>
	<script src="../plugins/calendar/js/responsive-calendar.js"></script>
	<script type="text/javascript">
      $(document).ready(function () {
        $(".responsive-calendar").responsiveCalendar({
          time: '2013-05',
          events: {
            "2013-04-30": {"number": 5, "url": "http://w3widgets.com/responsive-slider"},
            "2013-04-26": {"number": 1, "url": "http://w3widgets.com"}, 
            "2013-05-03":{"number": 1}, 
            "2013-06-12": {}}
        });
      });
      
      function addFoodItem(id,name,srp){
      	var i;
      	var flag=0;
      	var table = document.getElementById("pitems");
      	var tmt = document.getElementById("totamt");
      	var tqty =1;
      	var temptot=0;
      	for(i=0;i<table.rows.length;i++){
      		if(table.rows.item(i).cells.item(0).innerHTML == name + '<input type="hidden" name="foodname[]" class="foodname" value="'+id+'">'){
      			
      			table.rows.item(i).cells.item(1).innerHTML = parseInt(table.rows.item(i).cells.item(1).innerHTML) + 1;
      			table.rows.item(i).cells.item(1).innerHTML = table.rows.item(i).cells.item(1).innerHTML + "<input type='hidden' name='foodqty[]' class='foodqty' value='"+table.rows.item(i).cells.item(1).innerHTML+"'/>";
      			console.log(document.getElementsByClassName('foodprice')[i-1].value);
      			table.rows.item(i).cells.item(3).innerHTML = (parseFloat(document.getElementsByClassName('foodamount')[i-1].value)+parseFloat(document.getElementsByClassName('foodprice')[i-1].value)).toFixed(2);
      			table.rows.item(i).cells.item(3).innerHTML = table.rows.item(i).cells.item(3).innerHTML + "<input type='hidden' name='foodamount[]' class='foodamount' value='"+table.rows.item(i).cells.item(3).innerHTML+"' />";
      			flag++;
      		}else{

      		}
      	}
      	if(flag==0){
			// Create an empty <tr> element and add it to the 1st position of the table:
			var row = table.insertRow(-1);

			// Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
			var cell1 = row.insertCell(0);
			var cell2 = row.insertCell(1);
			var cell3 = row.insertCell(2);
			var cell4 = row.insertCell(3);
			var cell5 = row.insertCell(4);
			var cell6 = row.insertCell(5);

			// Add some text to the new cells:
			cell1.innerHTML = name + "<input type='hidden' name='foodname[]' class='foodname' value='"+id+"' />";
			cell2.innerHTML =1 + "<input type='hidden' name='foodqty[]' class='foodqty' value='1' />";
			cell3.innerHTML = srp + "<input type='hidden' name='foodprice[]' class='foodprice' value='"+srp+"' />";
			cell4.innerHTML = srp + "<input type='hidden' name='foodamount[]' class='foodamount' value='"+srp+"' />";
			//console.log(document.getElementsByClassName('foodqty')[0].value);
			cell5.innerHTML = "<button type='button' class='delfood' onclick=delFoodItem("+id+")>-</button>";
			cell6.innerHTML = "<input type='hidden' name='foodid[]' class='foodid' value='"+id+"' />";
      	}else{
  			flag=0;
      	}
      	for(i=0;i<table.rows.length;i++){
      		
      		if(i>0){
	      		temptot = temptot + Number(document.getElementsByClassName('foodamount')[i-1].value);
      		}
      	}
      	tmt.innerHTML = temptot.toFixed(2);
      	temptot = 0;
		
      }


      function delFoodItem(id){
      	//console.log(id);

      	var i;
      	var flag=0;
      	var table = document.getElementById("pitems");
      	var tmt = document.getElementById("totamt");
      	var tqty =1;
      	var temptot=0;
      	for(i=0;i<table.rows.length;i++){
      		if(table.rows.item(i).cells.item(5).innerHTML == '<input type="hidden" name="foodid[]" class="foodid" value="'+id+'">'){
      			
      			if(parseInt(table.rows.item(i).cells.item(1).innerHTML)==1){
      				var row = table.deleteRow(i);
      			}else{
      				table.rows.item(i).cells.item(1).innerHTML = parseInt(table.rows.item(i).cells.item(1).innerHTML) - 1;
	      			table.rows.item(i).cells.item(1).innerHTML = table.rows.item(i).cells.item(1).innerHTML + "<input type='hidden' name='foodqty[]' class='foodqty' value='"+table.rows.item(i).cells.item(1).innerHTML+"'/>";
	      			table.rows.item(i).cells.item(3).innerHTML = (parseFloat(document.getElementsByClassName('foodamount')[i-1].value)-parseFloat(document.getElementsByClassName('foodprice')[i-1].value)).toFixed(2);
	      			table.rows.item(i).cells.item(3).innerHTML = table.rows.item(i).cells.item(3).innerHTML + "<input type='hidden' name='foodamount[]' class='foodamount' value='"+table.rows.item(i).cells.item(3).innerHTML+"' />";
	      			
      			}
      			flag++;
      		}else{

      		}
      	}

      }
    </script>
</body>
</html>