<?php
  include '../inc/functions.php';
  $db = new pos_functions();
/*
  $d1=date_create("2015-05-01");
  $d2=date_create("2015-05-15");
  $diff=date_diff($d1,$d2);
  $days = $diff->format("%d");

  $income = array();
  $expense = array();
  $days = array();
  for($i=0;$i<$days;$i++){
    $d3 = date("2015-05-".($i+1));

    array_push($income,$db->getDailySales($d3));
    array_push($expense,$db->getDailyExpense($d3));
    array_push($days,$d3);*/
  }
  //
?>
<html>
  <head>
    <script type="text/javascript"
          src="../js/googlecharts.js"></script>

    <script type="text/javascript">
      google.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: 'Company Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="curve_chart" style="width: 900px; height: 500px"></div>
  </body>
</html>