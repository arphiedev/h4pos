<?php

	include '../inc/functions.php';
	$db = new pos_functions();


?>
<!DOCTYPE html>
<html>
<head>
	<title>H4 Point Of Sales</title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <!-- MetisMenu CSS -->
    <link href="../css/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="../plugins/calendar/css/responsive-calendar.css" rel="stylesheet" type="text/css" />
    <link href="../css/AdminLTE.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		    <?php $db->showHead(); ?>
		    <!-- /.navbar-header -->

		    <ul class="nav navbar-top-links navbar-right">
		        <!-- /.dropdown -->
		        <li class="dropdown">
		            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
		                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
		            </a>
		            <ul class="dropdown-menu dropdown-messages">
		                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
		                </li>
		                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
		                </li>
		                <li class="divider"></li>
		                <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
		                </li>
		            </ul>
		            <!-- /.dropdown-user -->
		        </li>
		        <!-- /.dropdown -->
		    </ul>
		    <!-- /.navbar-top-links -->

		    <div class="navbar-default sidebar" role="navigation">
		        <div class="sidebar-nav navbar-collapse">
		            <ul class="nav" id="side-menu">
		                <li class="sidebar-search">
		                    <div class="input-group custom-search-form">
		                        <input type="text" class="form-control" placeholder="Search...">
		                        <span class="input-group-btn">
		                        <button class="btn btn-default" type="button">
		                            <i class="fa fa-search"></i>
		                        </button>
		                    </span>
		                    </div>
		                    <!-- /input-group -->
		                </li>
		                <li>
		                    <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
		                </li>
		                <li>
		                    <a href="#"><i class="fa fa-cutlery fa-fw"></i> Sales<span class="fa arrow"></span></a>
		                    <ul class="nav nav-second-level">
		                        <li>
		                            <a href="addorder.php">Add Order</a>
		                        </li>
		                        <li>
		                            <a href="vieworders.php">View Orders</a>
		                        </li>
		                        <li>
		                            <a href="viewsales.php">View Sales</a>
		                        </li>
		                    </ul>
		                    <!-- /.nav-second-level -->
		                </li>
		               	<li>
		                    <a href="viewMenu.php"><i class="fa fa-file-text fa-fw"></i> Menu Management</a>
		                </li>
		                <li>
		                    <a href="#"><i class="fa fa-wrench fa-book"></i> Accounting and Statistics<span class="fa arrow"></span></a>
		                    <ul class="nav nav-second-level">
		                        <li>
		                            <a href="addexpense.php">Add Expense</a>
		                        </li>
		                        <li>
		                            <a href="addaccounttitle.php">Add Account Title</a>
		                        </li>
		                        <li>
		                            <a href="incomestatement.php">View Income Statement</a>
		                        </li>
		                        <li>
		                            <a href="stat.php">Statistics</a>
		                        </li>
		                    </ul>
		                    <!-- /.nav-second-level -->
		                </li>
		                <!--li>
		                    <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
		                    <ul class="nav nav-second-level">
		                        <li>
		                            <a href="#">Second Level Item</a>
		                        </li>
		                        <li>
		                            <a href="#">Second Level Item</a>
		                        </li>
		                        <li>
		                            <a href="#">Third Level <span class="fa arrow"></span></a>
		                            <ul class="nav nav-third-level">
		                                <li>
		                                    <a href="#">Third Level Item</a>
		                                </li>
		                                <li>
		                                    <a href="#">Third Level Item</a>
		                                </li>
		                                <li>
		                                    <a href="#">Third Level Item</a>
		                                </li>
		                                <li>
		                                    <a href="#">Third Level Item</a>
		                                </li>
		                            </ul>
		                           
		                        </li>
		                    </ul>
		                    
		                </li-->
		                <li>
		                    <a href="#"><i class="fa fa-users fa-fw"></i> User Management<span class="fa arrow"></span></a>
		                    
		                    <!-- /.nav-second-level -->
		                </li>
		            </ul>
		        </div>
		        <!-- /.sidebar-collapse -->
		    </div>
		    <!-- /.navbar-static-side -->
		</nav>
		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Sale Details for <?php if(isset($_GET['oid'])) print $db->getOrderDetail($_GET['oid'],1); ?> </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            CUSTOMER: <?php print $db->getOrderDetail($_GET['oid'],1); ?> <br />
            Date: <?php print $db->getOrderDetail($_GET['oid'],0); ?>
           	<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Food Ordered
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Quantity</th>
                                            <th>Food</th>
                                            <th>Price</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php $db->showFoodItems($_GET['oid']);?>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                   
                    <!-- /.panel -->
                </div>
            <!-- /.row -->
            
            <!-- /.row -->

        </div>

	</div>

	<script src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/sb-admin-2.js"></script>
	<script src="../js/metisMenu.min.js"></script>
	<script src="../plugins/calendar/js/responsive-calendar.js"></script>
	<script type="text/javascript">
      $(document).ready(function () {
        
      });
    </script>
</body>
</html>